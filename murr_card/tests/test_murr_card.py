import pytest
from django.urls import reverse
from rest_framework import status

from murr_card.models import MurrCard


pytestmark = [
    pytest.mark.django_db,
]


def test_murr_card_create(api_client, create_murren, create_category, murr_card_data):
    murren = create_murren
    category = create_category
    api_client.force_authenticate(user=murren)

    data = {
        **murr_card_data,
        'owner': murren.username,
        'category': category.slug
    }

    url = reverse('murr_card-list')
    response = api_client.post(url, data, format='json')
    murr_card = MurrCard.objects.first()
    murr_card_tags = murr_card.tags.values_list('name', flat=True)
    assert response.status_code == status.HTTP_201_CREATED
    assert MurrCard.objects.count() == 1
    assert murr_card.owner == murren
    assert murr_card.title == data['title']
    assert murr_card.content == data['content']
    assert murr_card.category == category
    assert all(tag in murr_card_tags for tag in data['tags'])


def test_murr_card_create_not_authenticated(create_murren, murr_card_data, api_client):
    murren = create_murren

    data = {
        **murr_card_data,
        'owner': murren.username,
    }

    url = reverse('murr_card-list')
    response = api_client.post(url, data=data, format='json')
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_murr_card_delete(create_release_murr, api_client):
    murren = create_release_murr['murren']
    api_client.force_authenticate(user=murren)

    assert MurrCard.objects.count() == 1

    url = reverse('murr_card-detail', kwargs={'pk': create_release_murr['murr_card'].id})
    response = api_client.delete(url)
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert MurrCard.objects.count() == 0


def test_murr_card_delete_not_authenticated(create_release_murr, api_client):
    url = reverse('murr_card-detail', kwargs={'pk': create_release_murr['murr_card'].id})
    response = api_client.delete(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.parametrize('count', [3])
def test_murr_card_list(create_release_murr_list, api_client):
    murren = create_release_murr_list['murren']
    api_client.force_authenticate(user=murren)

    url = reverse('murr_card-list')
    response = api_client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == MurrCard.objects.count()


@pytest.mark.parametrize('count', [3])
def test_murr_card_my_likes_list(create_release_murr_list, api_client):
    murr_cards = create_release_murr_list['murr_card_list']
    murren = create_release_murr_list['murren']
    api_client.force_authenticate(user=murren)

    assert MurrCard.objects.count() == 3

    url = reverse('murr_card-like', kwargs={'pk': murr_cards[0].id})
    assert api_client.post(url).status_code == 200

    url = reverse('murr_card-my-likes')
    response = api_client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == murren.liked_murrcards.count()


def test_murr_card_my_likes_list_not_authenticated(api_client):
    url = reverse('murr_card-my-likes')
    response = api_client.get(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_set_default_category(create_murren, create_category, murr_card_data, api_client):
    murren = create_murren
    category = create_category
    api_client.force_authenticate(user=murren)

    data = {
        **murr_card_data,
        'owner': murren.username,
        'category': '',
    }

    url = reverse('murr_card-list')
    response = api_client.post(url, data, format='json')
    murr_card = MurrCard.objects.first()
    assert response.status_code == status.HTTP_201_CREATED
    assert murr_card.category == category


@pytest.mark.parametrize('count', [1])
def test_murr_card_feed_list(create_murren_list, create_release_murr, api_client):
    murr_cards = create_release_murr['murr_card']
    murren_author = create_release_murr['murren']
    murren_subscriber = create_murren_list[0]

    murren_subscriber.subscriptions.add(murren_author)

    api_client.force_authenticate(user=murren_subscriber)

    url = reverse('murr_card-feed')

    response = api_client.get(url, format='json')
    assert response.status_code == status.HTTP_200_OK
    assert response.data['count'] == 1
    assert response.data['results'][0]['owner'] == murren_author.id

    murren_subscriber.subscriptions.remove(murren_author)

    response = api_client.get(url, format='json')
    assert response.status_code == status.HTTP_200_OK
    assert response.data['count'] == 0


def test_murr_card_feed_list_not_authenticated(api_client):
    url = reverse('murr_card-feed')
    response = api_client.get(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.parametrize('count', [3])
def test_murr_card_list_check_is_like_and_is_dislike_in_resp(
    create_release_murr_list, api_client
):
    murren = create_release_murr_list['murren']
    api_client.force_authenticate(user=murren)

    url = reverse('murr_card-list')
    response = api_client.get(url)
    resp_json = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert not any([murr['is_like'] for murr in resp_json['results']])
    assert not any([murr['is_dislike'] for murr in resp_json['results']])
