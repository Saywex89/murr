from channels.db import database_sync_to_async
from django.contrib.auth import get_user_model

from murr_websocket.consumers.base import BaseMurrWebSocketConsumer
from murr_websocket.models import MurrWebSocketMessage
from murren.serializers import MurrenSerializer

Murren = get_user_model()

LIMIT_MESSAGES_ON_PAGE = 30


class MurrChatConsumer(BaseMurrWebSocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scope['murr_ws_type'] = 'murr_chat'

    async def connect(self):
        await super().connect()
        self.murr_chat_name = self.scope['murr_ws_name']

        await self.channel_layer.group_add(self.murr_chat_name, self.channel_name)
        await self.add_murren_to_murr_websocket_members(self.scope['murren'], self.murr_websocket_instance.id)
        self.murr_ws_member = self.scope['murren'].murr_ws_member\
                                                  .filter(murr_ws_instance=self.murr_websocket_instance)\
                                                  .first()
        self.murr_ws_member.set_online(True)

        murr_websocket_data = await self.get_websocket_data(self.murr_websocket_instance.id)
        murr_websocket_data['murrens_in_ws_data'] = await self.get_online_murrens(murr_websocket_data)
        await self._group_send(murr_websocket_data, gan='join_to_murr_chat')

    async def get_online_murrens(self, ws_data):
        return list(filter(lambda murren: murren['is_online'], ws_data['murrens_in_ws_data']))

    async def gan__list_messages(self, event):
        offset = event['data'].get('offset', 0)
        messages = await self.get_messages(offset)
        return await self._send_message(messages, gan=event['gan'])

    @database_sync_to_async
    def get_messages(self, offset):
        messages = MurrWebSocketMessage.objects\
            .select_related('murr_ws_member')\
            .filter(murr_ws_instance=self.murr_websocket_instance)\
            .order_by('-id')
        data = {
            'messages': [],
            'offset': offset,
            'last_message': messages.count() - (LIMIT_MESSAGES_ON_PAGE + offset) < 0
        }
        for message in messages[offset:offset + LIMIT_MESSAGES_ON_PAGE:-1]:
            data['messages'].append({
                'id': message.id,
                'murren_name': message.murr_ws_member.username,
                'murren_avatar': MurrenSerializer.get_avatar(None, message.murr_ws_member),
                'message': message.murr_ws_message
            })
        return data

    async def disconnect(self, code):
        self.murr_ws_member.set_online(False)
        await self._group_send(self.scope['murren'].username,
                               gan='disconnect')
        await self.channel_layer.group_discard(self.murr_chat_name, self.channel_name)
        await super().disconnect(code=code)
