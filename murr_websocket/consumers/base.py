from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.contrib.auth import get_user_model

from murr_websocket.models import MurrWebSocketMessage, MurrWebSocket, MurrWebSocketType, MurrWebSocketMembers
from murren.serializers import MurrenSerializer

Murren = get_user_model()


class BaseMurrWebSocketConsumer(AsyncJsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.murr_websocket_instance = None

    async def connect(self):
        if 'user' not in self.scope:
            await self._send_message({'detail': 'Authorization failed'})
            await self.close(code=1000)
            return
        self.scope["murren"] = await self.get_murren(self.scope["user"])
        self.scope['murr_ws_name'] = self.scope['url_route']['kwargs']['murr_ws_name']
        murr_websocket_instance = await self.get_murr_websocket()
        if not murr_websocket_instance:
            await self._trow_error({'detail': 'murr_websocket not found'})
            await self.close()
            return
        self.murr_websocket_instance = murr_websocket_instance
        await self.accept()

    @database_sync_to_async
    def get_murren(self, pk):
        murren = Murren.objects.get(id=pk)
        return murren

    async def receive_json(self, content, **kwargs):
        message = await self.validate_content(content)
        if message:
            gan = message['gan']
            method = getattr(self, f'gan__{gan}', self.method_undefined)
            await method(message)
        else:
            await self._trow_error({'detail': 'Invalid message'})

    @classmethod
    async def validate_content(cls, content):
        if isinstance(content, dict) \
                and isinstance(content.get('gan'), str) \
                and isinstance(content.get('data'), dict):
            return content

    async def _group_send(self, data, gan=None):
        data = {'type': 'proxy.group.send', 'data': data, 'gan': gan}
        await self.channel_layer.group_send(self.scope['murr_ws_name'], data)

    async def proxy_group_send(self, gan):
        await self._send_message(gan['data'], gan=gan.get('gan'))

    async def method_undefined(self, message):
        await self._trow_error({'detail': 'Unknown gan'}, gan=message['gan'])

    async def _send_message(self, data, gan=None):
        await self.send_json(content={'status': 'ok', 'data': data, 'gan': gan})

    async def _trow_error(self, data, gan=None):
        await self.send_json(content={'status': 'error', 'data': data, 'gan': gan})

    @database_sync_to_async
    def get_murr_websocket(self):
        murr_websocket_instance = MurrWebSocket.objects.filter(murr_ws_name=self.scope['murr_ws_name'],
                                                               murr_ws_type=MurrWebSocketType(
                                                                   self.scope['murr_ws_type'])).first()
        return murr_websocket_instance

    async def gan__send_message(self, gan):
        message = gan['data'].get('message')
        if not message:
            return await self._trow_error({'detail': 'Missing message'}, gan=gan['gan'])
        await self.save_message(message, self.scope['murren'])
        data = {
            'murren_name': self.scope['murren'].username,
            'murren_avatar': MurrenSerializer.get_avatar(self.murr_websocket_instance, self.scope['murren']),
            'message': gan['data']['message'],
        }
        return await self._group_send(data, gan=gan['gan'])

    @database_sync_to_async
    def save_message(self, message, murren):
        m = MurrWebSocketMessage(murr_ws_member=murren, murr_ws_instance=self.murr_websocket_instance,
                                 murr_ws_message=message)
        m.save()

    async def gan__create_murr_websocket(self, gan):
        murr_ws_type = gan['data'].get('murr_ws_type')
        if not murr_ws_type:
            return await self._trow_error({'detail': 'Missing murr_ws_type'}, gan=gan['gan'])
        murr_ws_name = gan['data'].get('murr_ws_name')
        if not murr_ws_name:
            return await self._trow_error({'detail': 'Missing murr_ws_name'}, gan=gan['gan'])
        if gan['data'].get('custom_gan_name'):
            gan['gan'] = gan['data'].get('custom_gan_name')

        murr_websocket = await self.create_murr_websocket(self.scope['murren'], murr_ws_type, murr_ws_name)
        murr_websocket_data = await self.get_websocket_data(murr_websocket.pk)

        await self._group_send(murr_websocket_data, gan=gan['gan'])

    @database_sync_to_async
    def create_murr_websocket(self, murren, murr_ws_type, murr_ws_name):
        murr_websocket, _ = MurrWebSocket.objects.get_or_create(murr_ws_name=murr_ws_name,
                                                                murr_ws_type=MurrWebSocketType(murr_ws_type))
        murr_websocket.save()
        murr_websocket_member, _ = MurrWebSocketMembers.objects.get_or_create(murr_ws_member=murren,
                                                                              murr_ws_instance=murr_websocket)
        murr_websocket_member.save()
        return murr_websocket

    async def gan__join_to_murr_websocket(self, gan):
        murr_websocket_id = gan['data'].get('murr_websocket_id')
        if not murr_websocket_id:
            return await self._trow_error({'detail': 'Missing murr_websocket_id'}, gan=gan['gan'])
        if gan['data'].get('custom_gan_name'):
            gan['gan'] = gan['data'].get('custom_gan_name')
        await self.add_murren_to_murr_websocket_members(self.scope['murren'], murr_websocket_id)
        murr_websocket_data = await self.get_websocket_data(murr_websocket_id)
        await self._group_send(murr_websocket_data, gan=gan['gan'])

    @database_sync_to_async
    def add_murren_to_murr_websocket_members(self, murren_instance, murr_websocket_id):
        murren_joined, _ = MurrWebSocketMembers.objects.get_or_create(murr_ws_member=murren_instance,
                                                                      murr_ws_instance_id=murr_websocket_id)

    @database_sync_to_async
    def get_websocket_data(self, murr_websocket_id):
        murr_websocket = MurrWebSocket.objects.filter(pk=murr_websocket_id).first()
        murr_websocket_members = MurrWebSocketMembers.objects.filter(murr_ws_instance=murr_websocket)
        murrens_in_ws_data = []
        for murren in murr_websocket_members:
            murrens_in_ws_data.append({
                'murren_id': murren.murr_ws_member.id,
                'murren_name': murren.murr_ws_member.username,
                'murren_avatar': MurrenSerializer.get_avatar(self.murr_websocket_instance, murren.murr_ws_member),
                'is_online': murren.is_online,
            })
        return {'murr_websocket_id': murr_websocket.id, 'murr_ws_name': murr_websocket.murr_ws_name,
                'murrens_in_ws_data': murrens_in_ws_data, 'murr_ws_link': murr_websocket.link}

    async def gan__destroy_murr_websocket(self, gan):
        murr_websocket_id = gan['data'].get('murr_websocket_id')
        if not murr_websocket_id:
            return await self._trow_error({'detail': 'Missing murr_websocket_id'}, gan=gan['gan'])
        await self.destroy_murr_websocket(murr_websocket_id)
        await self._group_send(f'Вебсокет c id - {murr_websocket_id} удален.', gan=gan['gan'])

    @database_sync_to_async
    def destroy_murr_websocket(self, murr_websocket_id):
        murr_websocket = MurrWebSocket.objects.filter(pk=murr_websocket_id).first()
        murr_websocket.delete()
