from io import BytesIO

from PIL import Image

ACCESS_MIME_TYPES = ['image/jpeg', 'image/png', 'image/webp']


def make_resize_image(tmp_image, size=(500, 500)):
    if tmp_image is None:
        return None, 'Image file required'

    if tmp_image.content_type not in ACCESS_MIME_TYPES:
        return None, 'This not image format'

    image = Image.open(tmp_image, mode='r')
    if image.mode in ('RGBA', 'LA'):
        fill_color = '#1d4552'
        background = Image.new(image.mode[:-1], image.size, fill_color)
        background.paste(image, image.split()[-1])
        image = background
    image.thumbnail(size, Image.ANTIALIAS)

    buffer = BytesIO()
    image.save(buffer, format='jpeg', optimize=True, quality=95)
    image.close()

    return buffer.getvalue(), None


def make_crop_image(tmp_image, crop=(0, 0, 75, 75)):
    if tmp_image is None:
        return None, 'Image file required'

    if tmp_image.content_type not in ACCESS_MIME_TYPES:
        return None, 'This not image format'

    image = Image.open(tmp_image, mode='r')

    x = crop[0]
    y = crop[1]
    width = crop[2] + crop[0]
    height = crop[3] + crop[1]
    image = image.crop((x, y, width, height))

    buffer = BytesIO()
    image.save(buffer, format='jpeg', optimize=True, quality=30)
    image.close()

    return buffer, None
