import pytest
from django.contrib.auth import get_user_model
from django.core import mail
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

Murren = get_user_model()

pytestmark = [
    pytest.mark.django_db,
]


def test_create_murren(murren_data):
    username = murren_data['username']
    password = murren_data['password']
    api = APIClient(enforce_csrf_checks=True)

    assert Murren.objects.count() == 0

    url = reverse('murren-list')
    response = api.post(url, murren_data, format='json')
    resp_content = response.json()

    assert response.status_code == status.HTTP_201_CREATED
    assert Murren.objects.count() == 1
    assert len(mail.outbox) == 1

    murren = Murren.objects.get(id=resp_content['id'])
    assert murren.username == murren_data['username']
    assert murren.password is not None
    assert murren.is_active is False

    html_mail_body = mail.outbox[0].alternatives[0][0]
    assert username in html_mail_body
    raw_list = html_mail_body.split('___')
    uid, token = raw_list[1], raw_list[2]

    url = reverse('murren-activation')
    data = {
        'uid': uid,
        'token': token,
    }
    response = api.post(url, data, format='json')
    murren.refresh_from_db()
    assert response.status_code == 204
    assert murren.is_active is True

    data = {
        'username': username,
        'password': password,
    }
    url = reverse('obtain_jwt_token')
    response = api.post(url, data, format='json')

    assert response.status_code == 200
    assert 'token' in response.json()


@pytest.mark.parametrize('count', [2])
def test_subscribe_murren(create_murren_list, api_client):
    murren_list = create_murren_list
    api_client.force_authenticate(user=murren_list[0])

    url = reverse('murrens-subscribe', kwargs={'pk': murren_list[1].id})
    response = api_client.post(url)

    murren_list[0].refresh_from_db()
    murren_list[1].refresh_from_db()

    assert response.status_code == status.HTTP_200_OK
    assert response.data.get('subscribers_count') == 1
    assert response.data.get('is_subscriber') is True
    assert murren_list[1].in_subscribers(murren_list[0]) is True

    response = api_client.post(url)

    murren_list[0].refresh_from_db()
    murren_list[1].refresh_from_db()

    assert response.status_code == status.HTTP_200_OK
    assert response.data.get('subscribers_count') == 0
    assert response.data.get('is_subscriber') is False
    assert murren_list[1].in_subscribers(murren_list[0]) is False


@pytest.mark.parametrize('count', [2])
def test_subscribe_murren_not_authenticated(create_murren_list, api_client):
    murren_list = create_murren_list

    url = reverse('murrens-subscribe', kwargs={'pk': murren_list[1].id})
    response = api_client.post(url)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_subscribe_murren_to_self(create_murren, api_client):
    murren = create_murren
    api_client.force_authenticate(user=murren)

    url = reverse('murrens-subscribe', kwargs={'pk': murren.id})
    response = api_client.post(url)

    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.parametrize('count', [3])
def test_murren_my_subscriptions_list(create_murren_list, api_client):
    murren_list = create_murren_list
    api_client.force_authenticate(user=murren_list[0])

    url = reverse('murrens-subscribe', kwargs={'pk': murren_list[1].id})
    assert api_client.post(url).status_code == status.HTTP_200_OK

    url = reverse('murrens-subscriptions')
    response = api_client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == murren_list[0].subscriptions.count()


def test_murren_my_subscriptions_list_not_authenticated(api_client):
    url = reverse('murrens-subscriptions')
    response = api_client.get(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_create_murren_with_incorrect_email(murren_data):
    api = APIClient(enforce_csrf_checks=True)
    murren_data['email'] = 'test_murren@test-murren.com'
    url = reverse('murren-list')
    response = api.post(url, murren_data, format='json')
    assert response.status_code == 400
    assert response.json()['email'] == ['Your email service is not allowed for register']
