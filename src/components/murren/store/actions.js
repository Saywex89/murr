import axios from "axios";
const CancelToken = axios.CancelToken;
let axiosCancelRequest;

import {
  STATUS_200_OK,
  STATUS_401_UNAUTHORIZED,
} from "../../../utils/http_response_status.js";
import { handlerErrorAxios } from "../../../utils/helpres.js";

import {
  MURREN_FETCH_ME_PROFILE,
  MURREN_SET_PROFILE,
  MURREN_AVATAR_RESIZE,
  MURREN_AVATAR_UPDATE,
  MURREN_AVATAR_CANCEL_UPLOAD,
} from "./type.js";

const handlerSuccessFetchProfile = ({ commit, data, status }) => {
  let result = { success: true, data, status };

  if (status === STATUS_401_UNAUTHORIZED) {
    return { ...result, success: false };
  }

  if (status === STATUS_200_OK) {
    commit(MURREN_SET_PROFILE, data);

    return result;
  }

  return { ...result, success: false };
};

export default {
  async [MURREN_FETCH_ME_PROFILE]({ commit, getters }) {
    try {
      const jwtToken = getters.accessToken_getters;

      const { data, status } = await axios.get(`/api/murren/profile/`, {
        // todo: Fix - send to global instance of axios
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
      });

      return handlerSuccessFetchProfile({ commit, data, status });
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURREN_AVATAR_RESIZE]({ getters }, { avatar }) {
    try {
      const jwtToken = getters.accessToken_getters;

      const formData = new FormData();
      /* tmp_image_name.jpeg - PIL requires the name of the file with the extension. Not used anywhere else */
      formData.append("avatar", avatar, "tmp_image_name.jpeg");

      const { status, data } = await axios.post(
        "/api/murren/avatar_resize/",
        formData,
        {
          responseType: "blob",
          headers: {
            Authorization: `Bearer ${jwtToken}`,
            "Content-Type": "multipart/form-data",
          },
          cancelToken: new CancelToken((c) => (axiosCancelRequest = c)),
        }
      );

      let result = { success: true, data, status };

      if (status === STATUS_401_UNAUTHORIZED) {
        return { ...result, success: false };
      }

      if (status === STATUS_200_OK) {
        return result;
      }

      axiosCancelRequest = null;

      return { ...result, success: false };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURREN_AVATAR_UPDATE]({ getters, commit }, { avatar, crop }) {
    try {
      const jwtToken = getters.accessToken_getters;

      const formData = new FormData();
      /* tmp_image_name.jpeg - PIL requires the name of the file with the extension. Not used anywhere else */
      formData.append("avatar", avatar, "tmp_image_name.jpeg");
      formData.append("crop.x", Math.round(crop.x));
      formData.append("crop.y", Math.round(crop.y));
      formData.append("crop.width", Math.round(crop.width));
      formData.append("crop.height", Math.round(crop.height));

      const { status, data } = await axios.patch(
        "/api/murren/avatar_update/",
        formData,
        {
          headers: {
            Authorization: `Bearer ${jwtToken}`,
            "Content-Type": "multipart/form-data",
          },
          cancelToken: new CancelToken((c) => (axiosCancelRequest = c)),
        }
      );

      axiosCancelRequest = null;

      return handlerSuccessFetchProfile({ commit, data, status });
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  [MURREN_AVATAR_CANCEL_UPLOAD]() {
    if (typeof axiosCancelRequest === "function") {
      axiosCancelRequest();
      axiosCancelRequest = null;
    }
  },
};
