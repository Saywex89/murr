import * as type from "./type.js";

export default {
  [type.MURR_CARDS_APPEND]: (state, payload) => {
    state.murrList = [...state.murrList, ...payload.results];
    state.murrListCount = payload.count;
  },
  [type.MURR_CARDS_CLEAR]: (state) => (state.murrList = []),
  [type.MURR_CARDS_REMOVE]: (state, murrID) => {
    state.murrList = state.murrList.filter((item) => item.id !== murrID);
    state.murrListCount = state.murrListCount - 1;
  },
  [type.MURR_CARDS_CHANGE_SUBSCRIBER_STATUS]: (state, { murrenID, isSubscriber }) => {
    state.murrList = state.murrList.map((item) => {
      if (item.owner === murrenID) {
        item.is_subscriber = isSubscriber;
      }
      return item;
    });
  },
  [type.MURR_CARD_SET]: (state, payload) => (state.murr = payload),
  [type.MURR_CARD_CLEAR]: (state) => (state.murr = null),
};
